import unittest
import GLXCurses


class TestDividable(unittest.TestCase):

    def test_get_child_x_coordinates(self):
        child_1 = GLXCurses.ChildElement()
        child_1.widget = GLXCurses.Widget()
        child_1.widget.preferred_width = 7
        child_1.properties = GLXCurses.ChildProperty()
        child_1.properties.expand = True
        child_1.properties.position = 0

        child_2 = GLXCurses.ChildElement()
        child_2.widget = GLXCurses.Widget()
        child_2.widget.preferred_width = 7
        child_2.properties = GLXCurses.ChildProperty()
        child_2.properties.expand = False
        child_2.properties.position = 1

        child_3 = GLXCurses.ChildElement()
        child_3.widget = GLXCurses.Widget()
        child_3.widget.preferred_width = 42
        child_3.properties = GLXCurses.ChildProperty()
        child_3.properties.expand = True
        child_3.properties.position = 2

        children = [
            child_1,
            child_2,
            child_3,
        ]

        self.assertEqual({0: {'start': 0, 'stop': 16},
                          1: {'start': 17, 'stop': 24},
                          2: {'start': 25, 'stop': 42}},
                         GLXCurses.Dividable.get_child_x_coordinates(children=children, length=42))

    def test_get_child_y_coordinates(self):
        child_1 = GLXCurses.ChildElement()
        child_1.widget = GLXCurses.Widget()
        child_1.widget.preferred_height = 7
        child_1.properties = GLXCurses.ChildProperty()
        child_1.properties.expand = True
        child_1.properties.position = 0

        child_2 = GLXCurses.ChildElement()
        child_2.widget = GLXCurses.Widget()
        child_2.widget.preferred_height = 7
        child_2.properties = GLXCurses.ChildProperty()
        child_2.properties.expand = False
        child_2.properties.position = 1

        child_3 = GLXCurses.ChildElement()
        child_3.widget = GLXCurses.Widget()
        child_3.widget.preferred_height = 42
        child_3.properties = GLXCurses.ChildProperty()
        child_3.properties.expand = True
        child_3.properties.position = 2

        children = [
            child_1,
            child_2,
            child_3,
        ]

        self.assertEqual({0: {'start': 0, 'stop': 16},
                          1: {'start': 17, 'stop': 24},
                          2: {'start': 25, 'stop': 42}},
                         GLXCurses.Dividable.get_child_y_coordinates(children=children, length=42))


if __name__ == "__main__":
    unittest.main()
