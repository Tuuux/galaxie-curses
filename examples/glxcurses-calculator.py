#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Curses Team, all rights reserved
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

import GLXCurses
import curses
import logging

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    # Create the main Application
    app = GLXCurses.Application()

    # Create a Menu
    menu = GLXCurses.MenuBar()
    menu.info_label = "GLXCurses Calculator"

    # The main entry
    entrybuffer = GLXCurses.EntryBuffer()

    entry = GLXCurses.Entry()
    entry.set_buffer(entrybuffer)
    entry.set_text("0")

    # Button
    button_0 = GLXCurses.Button()
    button_0.text = "0"

    button_1 = GLXCurses.Button()
    button_1.text = "1"

    button_2 = GLXCurses.Button()
    button_2.text = "2"

    button_3 = GLXCurses.Button()
    button_3.text = "3"

    button_4 = GLXCurses.Button()
    button_4.text = "4"

    button_5 = GLXCurses.Button()
    button_5.text = "5"

    button_6 = GLXCurses.Button()
    button_6.text = "6"

    button_7 = GLXCurses.Button()
    button_7.text = "7"

    button_8 = GLXCurses.Button()
    button_8.text = "8"

    button_9 = GLXCurses.Button()
    button_9.text = "9"

    button_division = GLXCurses.Button()
    button_division.text = "/"

    button_multiply = GLXCurses.Button()
    button_multiply.text = "x"

    button_subtract = GLXCurses.Button()
    button_subtract.text = "-"

    button_add = GLXCurses.Button()
    button_add.text = "+"

    button_dot = GLXCurses.Button()
    button_dot.text = "."

    button_percent = GLXCurses.Button()
    button_percent.text = "%"

    button_undo = GLXCurses.Button()
    button_undo.text = "<-"

    button_group_open = GLXCurses.Button()
    button_group_open.text = "("

    button_group_close = GLXCurses.Button()
    button_group_close.text = ")"

    button_square = GLXCurses.Button()
    button_square.text = "x2"

    button_square = GLXCurses.Button()
    button_square.text = "V"
    # Create a new Horizontal Box container

    hbox_line1 = GLXCurses.HBox()
    hbox_line1.spacing = 0
    hbox_line1.pack_end(button_7)
    hbox_line1.pack_end(button_8)
    hbox_line1.pack_end(button_9)
    hbox_line1.pack_end(button_division)

    hbox_line2 = GLXCurses.HBox()
    hbox_line2.spacing = 0
    hbox_line2.pack_end(button_4)
    hbox_line2.pack_end(button_5)
    hbox_line2.pack_end(button_6)
    hbox_line2.pack_end(button_multiply)

    hbox_line3 = GLXCurses.HBox()
    hbox_line3.spacing = 0
    hbox_line3.pack_end(button_1)
    hbox_line3.pack_end(button_2)
    hbox_line3.pack_end(button_3)
    hbox_line3.pack_end(button_subtract)

    hbox_line4 = GLXCurses.HBox()
    hbox_line4.spacing = 0
    hbox_line4.pack_end(button_0)
    hbox_line4.pack_end(button_dot)
    hbox_line4.pack_end(button_percent)
    hbox_line4.pack_end(button_add)

    # Create a Horizontal Separator and a Label
    hline = GLXCurses.HSeparator()

    label_press_q = GLXCurses.Label()
    label_press_q.text = "Press \"q\" key to exit ... What about you arrows's key's"
    label_press_q.set_single_line_mode(True)
    label_press_q.set_justify("center")
    label_press_q.xalign = 0.5
    label_press_q.yalign = 0.3
    label_press_q.override_color("yellow")

    # Create a main Vertical Box
    vbox_main = GLXCurses.VBox()
    vbox_main.pack_end(entry)
    vbox_main.pack_end(hbox_line1)
    vbox_main.pack_end(hbox_line2)
    vbox_main.pack_end(hbox_line3)
    vbox_main.pack_end(hbox_line4)

    # Create the main Window
    win_main = GLXCurses.Window()
    win_main.add(vbox_main)

    # Create a Status Bar
    statusbar = GLXCurses.StatusBar()
    signal_context_id = statusbar.get_context_id("SIGNAL")
    curses_context_id = statusbar.get_context_id("CURSES")

    def handle_keys(self, event_signal, *event_args):
        statusbar.remove_all(curses_context_id)
        statusbar.push(curses_context_id, "HANDLE KEY: " + str(event_args[0]))

        if app.has_focus is None:

            if event_args[0] == curses.KEY_F1:
                pass
            if event_args[0] == curses.KEY_F2:
                entry.set_visibility(not entry.visibility)
            if event_args[0] == curses.KEY_F3:
                entry.can_focus = not entry.can_focus
                entry.set_editable(not entry.get_editable())
            if event_args[0] == curses.KEY_F4:
                pass
            if event_args[0] == curses.KEY_F5:
                pass
            if event_args[0] == curses.KEY_F6:
                pass
            if event_args[0] == curses.KEY_F7:
                pass
            if event_args[0] == curses.KEY_F8:
                pass
            if event_args[0] == curses.KEY_F9:
                pass
            if event_args[0] == curses.KEY_F10:
                # Everything have a end, the main loop too ...
                GLXCurses.mainloop.stop()

            # Key c
            if event_args[0] == 99 or event_args[0] == 263:
                remove_prelight()
                entry.set_text("0")

            # Key Enter
            if event_args[0] == 10:
                # Make teh compute here
                remove_prelight()
                statusbar.push(curses_context_id, str(eval(entry.get_text())))
                entry.set_text(str(eval(entry.get_text())))
            # Key %
            if event_args[0] == 37:
                remove_prelight()
                button_percent.state["PRELIGHT"] = True
                entry.set_text(entry.get_text() + str("%"))
            # Key +
            if event_args[0] == 42:
                remove_prelight()
                button_multiply.state["PRELIGHT"] = True
                entry.set_text(entry.get_text() + str("*"))
            # Key +
            if event_args[0] == 43:
                remove_prelight()
                button_add.state["PRELIGHT"] = True
                entry.set_text(entry.get_text() + str("+"))
            # Key -
            if event_args[0] == 45:
                remove_prelight()
                button_subtract.state["PRELIGHT"] = True
                entry.set_text(entry.get_text() + str("-"))
            # Key .
            if event_args[0] == 46:
                remove_prelight()
                button_dot.state["PRELIGHT"] = True
                entry.set_text(entry.get_text() + str("."))
            # Key /
            if event_args[0] == 47:
                remove_prelight()
                button_division.state["PRELIGHT"] = True
                entry.set_text(entry.get_text() + str("/"))
            # Key num 0
            if event_args[0] == 48:
                remove_prelight()
                button_0.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(0))
                else:
                    entry.set_text(entry.get_text() + str(0))
            # Key num 1
            if event_args[0] == 49:
                remove_prelight()
                button_1.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(1))
                else:
                    entry.set_text(entry.get_text() + str(1))

            # Key num 2
            if event_args[0] == 50:
                remove_prelight()
                button_2.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(2))
                else:
                    entry.set_text(entry.get_text() + str(2))
            # Key num 3
            if event_args[0] == 51:
                remove_prelight()
                button_3.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(3))
                else:
                    entry.set_text(entry.get_text() + str(3))
            # Key num 4
            if event_args[0] == 52:
                remove_prelight()
                button_4.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(4))
                else:
                    entry.set_text(entry.get_text() + str(4))
            # Key num 5
            if event_args[0] == 53:
                remove_prelight()
                button_5.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(5))
                else:
                    entry.set_text(entry.get_text() + str(5))
            # Key num 6
            if event_args[0] == 54:
                remove_prelight()
                button_6.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(6))
                else:
                    entry.set_text(entry.get_text() + str(6))
            # Key num 7
            if event_args[0] == 55:
                remove_prelight()
                button_7.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(7))
                else:
                    entry.set_text(entry.get_text() + str(7))
            # Key num 8
            if event_args[0] == 56:
                remove_prelight()
                button_8.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(8))
                else:
                    entry.set_text(entry.get_text() + str(8))
            # Key num 9
            if event_args[0] == 57:
                remove_prelight()
                button_9.state["PRELIGHT"] = True
                if entry.get_text() == str(0):
                    entry.set_text(str(9))
                else:
                    entry.set_text(entry.get_text() + str(9))

            # Keyboard temporary thing
            if event_args[0] == ord("q"):
                # Everything have a end, the main loop too ...
                GLXCurses.mainloop.stop()

    def remove_prelight():
        for item in {
            button_0,
            button_1,
            button_2,
            button_3,
            button_4,
            button_5,
            button_6,
            button_7,
            button_8,
            button_9,
            button_division,
            button_multiply,
            button_add,
            button_subtract,
            button_dot,
            button_percent,
        }:
            if item.state["PRELIGHT"] is not False:
                item.state["PRELIGHT"] = False

    def on_click(self, event_signal, event_args=None):
        remove_prelight()
        if event_args is None:
            event_args = dict()
        if event_args["id"] == entry.id:
            statusbar.remove_all(signal_context_id)
            statusbar.push(signal_context_id, event_args["name"] + " " + event_signal)

    def signal_event(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()

        # Crash AUTO
        # statusbar.push(
        #    signal_context_id, "{0}: {1}".format(event_signal, event_args)
        # )

    # Add Everything inside the Application
    app.menubar = menu
    app.add_window(win_main)
    app.statusbar = statusbar

    # Event's and Signals
    app.connect("BUTTON1_CLICKED", on_click)  # Mouse Button
    # app.connect('BUTTON1_RELEASED', on_click)  # Mouse Button
    # app.connect('MOUSE_EVENT', on_click)  # Mouse Button
    app.connect("CURSES", handle_keys)  # Keyboard
    app.connect("SIGNALS", signal_event)  # Something it emit a signal

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
