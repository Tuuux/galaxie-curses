#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXCurses
import curses
import logging
from GLXCurses import GLXC

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = "Tuux"

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    # Create the main Application
    app = GLXCurses.Application()

    # Create a Menu
    menu = GLXCurses.MenuBar()
    menu.info_label = "GLXCurses Separator's Demo"

    # Create Buttons
    Button1 = GLXCurses.Button()
    Button1.text = "Button"

    RadioButton1 = GLXCurses.RadioButton()
    RadioButton1.text = "RadioButton"

    CheckButton1 = GLXCurses.CheckButton()
    CheckButton1.text = "CheckButton"

    # Create a Horizontal Separator and a Label
    hline1 = GLXCurses.HSeparator()
    hline1.position_type = "BOTTOM"
    hline2 = GLXCurses.HSeparator()
    hline2.position_type = "TOP"

    vline1 = GLXCurses.VSeparator()
    vline2 = GLXCurses.VSeparator()
    # vline2.justify = "LEFT"
    # vline1.justify = "RIGHT"

    # Create a new Horizontal Box contener
    hbox = GLXCurses.HBox()
    hbox.spacing = 1

    hbox.pack_end(Button1, False)
    hbox.pack_end(vline1, False)
    hbox.pack_end(RadioButton1, False)
    hbox.pack_end(vline2, False)
    hbox.pack_end(CheckButton1, True)

    vline3 = GLXCurses.VSeparator()

    label_press_q = GLXCurses.Label()
    label_press_q.text = 'Press "q" key to exit ...'
    label_press_q.xalign = 0.5
    label_press_q.yalign = 0.3
    label_press_q.override_color("yellow")

    # Create a main Vertical Box
    vbox_main = GLXCurses.VBox()
    vbox_main.pack_end(hline1)
    vbox_main.pack_end(hbox)
    vbox_main.pack_end(hline2)
    vbox_main.pack_end(label_press_q)

    # Create the main Window
    win_main = GLXCurses.Window()
    win_main.add(vbox_main)

    # Create a Status Bar
    statusbar = GLXCurses.StatusBar()
    context_id = statusbar.get_context_id("example")

    def handle_keys(self, event_signal, *event_args):
        logging.debug("HANDLE KEY: " + str(event_args[0]))

        if event_args[0] == curses.KEY_F5:
            app.has_focus = Button1

        if event_args[0] == curses.KEY_F6:
            Button1.sensitive = not Button1.sensitive

        # Keyboard temporary thing
        if event_args[0] == ord("q"):
            # Everything have a end, the main loop too ...
            GLXCurses.mainloop.stop()

    def on_click(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()
        statusbar.push(context_id, "")
        if event_args["id"] == Button1.id:
            statusbar.push(context_id, event_args["label"] + " " + event_signal)
        if event_args["id"] == RadioButton1.id:
            if RadioButton1.active:
                statusbar.push(context_id, RadioButton1.text + " " + "is active")
            else:
                statusbar.push(
                    context_id, RadioButton1.text + " " + "is not active"
                )

        if event_args["id"] == CheckButton1.id:
            if CheckButton1.active:
                statusbar.push(context_id, CheckButton1.text + " " + "is active")
            else:
                statusbar.push(
                    context_id, CheckButton1.text + " " + "is not active"
                )

    # Add Everything inside the Application
    app.menubar = menu
    app.add_window(win_main)
    app.statusbar = statusbar
    # Signal
    app.connect("BUTTON1_CLICKED", on_click)
    app.connect("BUTTON1_RELEASED", on_click)
    app.connect("CURSES", handle_keys)

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
