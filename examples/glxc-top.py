#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Curses Team, all rights reserved
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

import GLXCurses
import curses
import logging

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")


    # Create the main Application
    app = GLXCurses.Application()

    # Create a Menu
    menu_left = GLXCurses.Menu()
    menu_left.title = "_Left"

    menuitem1 = GLXCurses.MenuItem()
    menuitem1.label = "_File listing"

    menuitem2 = GLXCurses.MenuItem()
    menuitem2.label = "Quick view"
    menuitem2.text_short_cut = "Ctrl + q"

    menuitem3 = GLXCurses.MenuItem()
    menuitem3.label = "Info"
    menuitem3.text_short_cut = "Ctrl + i"

    menuitem4 =  GLXCurses.MenuItem()
    menuitem4.label = "Tree"

    menuitem5 = GLXCurses.HSeparator()

    menuitem6 = GLXCurses.MenuItem()
    menuitem6.label = "Listing format..."

    menuitem7 = GLXCurses.MenuItem()
    menuitem7.label = "Sort order..."

    menuitem8 = GLXCurses.MenuItem()
    menuitem8.label = "Filter..."

    menuitem9 = GLXCurses.MenuItem()
    menuitem9.label = "Encoding..."

    menuitem10 = GLXCurses.HSeparator()

    menuitem11 = GLXCurses.MenuItem()
    menuitem11.label = "FTP link..."

    menuitem12 = GLXCurses.MenuItem()
    menuitem12.label = "Shell link..."

    menuitem13 = GLXCurses.MenuItem()
    menuitem13.label = "SFTP link..."

    menuitem14 = GLXCurses.MenuItem()
    menuitem14.label = "Panelize"

    menuitem15 = GLXCurses.HSeparator()

    menuitem16 = GLXCurses.MenuItem()
    menuitem16.label = "Rescan"
    menuitem16.text_short_cut = "Ctrl + r"

    menu_left.pack_end(menuitem1)
    menu_left.pack_end(menuitem2)
    menu_left.pack_end(menuitem3)
    menu_left.pack_end(menuitem4)
    menu_left.pack_end(menuitem5)
    menu_left.pack_end(menuitem6)
    menu_left.pack_end(menuitem7)
    menu_left.pack_end(menuitem8)
    menu_left.pack_end(menuitem9)
    menu_left.pack_end(menuitem10)
    menu_left.pack_end(menuitem11)
    menu_left.pack_end(menuitem12)
    menu_left.pack_end(menuitem13)
    menu_left.pack_end(menuitem14)
    menu_left.pack_end(menuitem15)
    menu_left.pack_end(menuitem16)

    menu_file = GLXCurses.Menu()
    menu_file.title = "_File"

    menuitem17 = GLXCurses.MenuItem()
    menuitem17.label = "_View"
    menuitem17.text_short_cut = "F3"

    menuitem18 = GLXCurses.MenuItem()
    menuitem18.label = "Vie_w file..."

    menuitem19 = GLXCurses.MenuItem()
    menuitem19.label = "_Filtered view"
    menuitem19.text_short_cut = "M-!"

    menuitem20 = GLXCurses.MenuItem()
    menuitem20.label = "_Edit"
    menuitem20.text_short_cut = "F4"

    menuitem21 = GLXCurses.MenuItem()
    menuitem21.label = "_Copy"
    menuitem21.text_short_cut = "F5"

    menuitem22 = GLXCurses.MenuItem()
    menuitem22.label = "C_hmod"
    menuitem22.text_short_cut = "C-x c"

    menuitem23 = GLXCurses.MenuItem()
    menuitem23.label = "_Link"
    menuitem23.text_short_cut = "C-x l"

    menuitem24 = GLXCurses.MenuItem()
    menuitem24.label = "_Symlink"
    menuitem24.text_short_cut = "C-x s"

    menuitem25 = GLXCurses.MenuItem()
    menuitem25.label = "Relative symlin_k"
    menuitem25.text_short_cut = "C-x v"

    menuitem26 = GLXCurses.MenuItem()
    menuitem26.label = "Edit s_ymlink"
    menuitem26.text_short_cut = "C-x C-s"

    menuitem27 = GLXCurses.MenuItem()
    menuitem27.label = "Ch_own"
    menuitem27.text_short_cut = "C-x o"

    menuitem28 = GLXCurses.MenuItem()
    menuitem28.label = "Advanced chown"

    menuitem29 = GLXCurses.MenuItem()
    menuitem29.label = "_Rename/Move"
    menuitem29.text_short_cut = "F6"

    menuitem30 = GLXCurses.MenuItem()
    menuitem30.label = "_Mkdir"
    menuitem30.text_short_cut = "F7"

    menuitem31 = GLXCurses.MenuItem()
    menuitem31.label = "_Delete"
    menuitem31.text_short_cut = "F8"

    menuitem32 = GLXCurses.MenuItem()
    menuitem32.label = "_Quick cd"
    menuitem32.text_short_cut = "M-c"

    menuitem33 = GLXCurses.HSeparator()

    menuitem34 = GLXCurses.MenuItem()
    menuitem34.label = "Select _group"
    menuitem34.text_short_cut = "+"

    menuitem35 = GLXCurses.MenuItem()
    menuitem35.label = "U_nselect group"
    menuitem35.text_short_cut = "-"

    menuitem36 = GLXCurses.MenuItem()
    menuitem36.label = "_Invert selection"
    menuitem36.text_short_cut = "*"

    menuitem37 = GLXCurses.HSeparator()

    menuitem38 = GLXCurses.MenuItem()
    menuitem38.label = "E_xit"
    menuitem38.text_short_cut = "F10"

    menu_file.pack_end(menuitem17)
    menu_file.pack_end(menuitem18)
    menu_file.pack_end(menuitem19)
    menu_file.pack_end(menuitem20)
    menu_file.pack_end(menuitem21)
    menu_file.pack_end(menuitem22)
    menu_file.pack_end(menuitem23)
    menu_file.pack_end(menuitem24)
    menu_file.pack_end(menuitem25)
    menu_file.pack_end(menuitem26)
    menu_file.pack_end(menuitem27)
    menu_file.pack_end(menuitem28)
    menu_file.pack_end(menuitem29)
    menu_file.pack_end(menuitem30)
    menu_file.pack_end(menuitem31)
    menu_file.pack_end(menuitem32)
    menu_file.pack_end(menuitem33)
    menu_file.pack_end(menuitem34)
    menu_file.pack_end(menuitem35)
    menu_file.pack_end(menuitem36)
    menu_file.pack_end(menuitem37)
    menu_file.pack_end(menuitem38)


    menu_command = GLXCurses.Menu()
    menu_command.title = "_Command"

    menu_option = GLXCurses.Menu()
    menu_option.title = "_Options"

    menu_right = GLXCurses.Menu()
    menu_right.title = "_Right"

    menubar = GLXCurses.MenuBar()
    menubar.info_label = "Galaxie Htop - v0.1a"
    menubar.pack_end(menu_left)
    menubar.pack_end(menu_file)
    menubar.pack_end(menu_command)
    menubar.pack_end(menu_option)
    menubar.pack_end(menu_right)

    label = GLXCurses.Label().new("Do you want to quit Galaxie Top ?")
    label.xalign = 0.5
    label.yalign = 0.5

    # Create the main Window
    dialog_quit = GLXCurses.Dialog()
    dialog_quit.add(label)
    dialog_quit.title = "Galaxie Top"
    dialog_quit.add_buttons("Yes", "1", "No", "2")
    dialog_quit.set_default_response("1")

    label_help = GLXCurses.Label().new("Galaxie Top v0.1a")
    label_help.set_single_line_mode(False)
    label_help.yalign = 0.5
    label_help.xalign = 0.5

    dialog_help = GLXCurses.Dialog()
    dialog_help.add(label_help)
    dialog_help.title = "About"
    dialog_help.add_buttons("Ok", "3")
    dialog_help.set_default_response("3")

    # Create the FileChooser
    filechooser = GLXCurses.Top()
    filechooser.set_decorated(True)

    hbox_main = GLXCurses.HBox()
    hbox_main.set_decorated(False)
    hbox_main.pack_start(filechooser)

    # Create a main Vertical Box
    # vbox_main = GLXCurses.VBox()
    # vbox_main.set_decorated(False)
    # vbox_main.pack_start(filechooser)

    # Create the main Window
    win_main = GLXCurses.Window()
    win_main.set_decorated(False)
    win_main.add(hbox_main)

    toolbar = GLXCurses.ToolBar()
    toolbar.labels = [
        "Help",
        "Setup",
        "Search",
        "Filter",
        "Tree",
        "SortBy",
        "Nice -",
        "Nice +",
        "Kill",
        "Quit",
    ]

    # Create a Status Bar
    statusbar = GLXCurses.StatusBar()
    signal_context_id = statusbar.get_context_id("SIGNAL")
    curses_context_id = statusbar.get_context_id("CURSES")

    messagebar = GLXCurses.MessageBar()
    message_context_id = messagebar.get_context_id("message")
    messagebar.push(message_context_id, "Welcome to Mini Commander")


    def handle_keys(self, event_signal, *event_args):
        statusbar.remove_all(curses_context_id)
        statusbar.push(curses_context_id, "HANDLE KEY: " + str(event_args[0]))

        if app.has_focus is None:
            app.has_default = toolbar
            app.has_focus = toolbar
            app.has_prelight = toolbar

        if event_args[0] == ord("q"):
            # Everything have a end, the main loop too ...
            GLXCurses.mainloop.stop()

        if event_args[0] == dialog_quit.get_widget_for_response("1").id:
            dialog_quit.close()
            GLXCurses.mainloop.stop()

        if event_args[0] == dialog_quit.get_widget_for_response("2").id:
            dialog_quit.close()

        if event_args[0] == dialog_help.get_widget_for_response("3").id:
            dialog_help.close()


    def response(self, event_signal, event_args=None):
        if event_args["id"] == dialog_quit.get_widget_for_response("1").id:
            dialog_quit.close()
            GLXCurses.mainloop.stop()

        if event_args["id"] == dialog_quit.get_widget_for_response("2").id:
            dialog_quit.close()

        if event_args["id"] == dialog_help.get_widget_for_response("3").id:
            dialog_help.close()


    def on_click(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()

        statusbar.remove_all(curses_context_id)
        statusbar.push(curses_context_id, "ON CLICK: " + str(event_args))

        if event_args["id"] == dialog_quit.get_widget_for_response("1").id:
            dialog_quit.close()
            GLXCurses.mainloop.stop()

        if event_args["id"] == dialog_quit.get_widget_for_response("2").id:
            dialog_quit.close()

        if event_args["id"] == dialog_help.get_widget_for_response("3").id:
            dialog_help.close()


    def f1_pressed(self, event_signal, event_args=None):
        dialog_help.run()


    def f2_pressed(self, event_signal, event_args=None):
        pass


    def f3_pressed(self, event_signal, event_args=None):
        filechooser.set_decorated(not filechooser.get_decorated())


    def f4_pressed(self, event_signal, event_args=None):
        pass


    def f5_pressed(self, event_signal, event_args=None):
        pass


    def f6_pressed(self, event_signal, event_args=None):
        pass


    def f7_pressed(self, event_signal, event_args=None):
        pass


    def f8_pressed(self, event_signal, event_args=None):
        pass


    def f9_pressed(self, event_signal, event_args=None):
        app.has_focus = menubar
        app.has_default = menubar
        app.has_prelight = menubar


    def f10_pressed(self, event_signal, event_args=None):
        dialog_quit.run()


    def button_release_event(self, event_signal, event_args=None):
        if event_args["id"] == menuitem38.id:
            dialog_quit.run()

    def signal_event(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()

        # Crash AUTO
        # statusbar.push(
        #    signal_context_id, "{0}: {1}".format(event_signal, event_args)
        # )


    # Add Everything inside the Application
    app.menubar = menubar
    app.add_window(dialog_quit)
    app.add_window(dialog_help)
    app.add_window(win_main)

    app.statusbar = statusbar
    app.messagebar = messagebar
    app.toolbar = toolbar
    app.has_default = toolbar
    app.has_focus = toolbar
    app.has_prelight = toolbar

    # Event's and Signals
    app.connect("F1_PRESSED", f1_pressed)  # ToolBar
    app.connect("F2_PRESSED", f2_pressed)  # ToolBar
    app.connect("F3_PRESSED", f3_pressed)  # ToolBar
    app.connect("F4_PRESSED", f4_pressed)  # ToolBar
    app.connect("F5_PRESSED", f5_pressed)  # ToolBar
    app.connect("F6_PRESSED", f6_pressed)  # ToolBar
    app.connect("F7_PRESSED", f7_pressed)  # ToolBar
    app.connect("F8_PRESSED", f8_pressed)  # ToolBar
    app.connect("F9_PRESSED", f9_pressed)  # ToolBar
    app.connect("F10_PRESSED", f10_pressed)  # ToolBar

    app.connect("F1_CLICKED", f1_pressed)  # ToolBar
    app.connect("F2_CLICKED", f2_pressed)  # ToolBar
    app.connect("F3_CLICKED", f3_pressed)  # ToolBar
    app.connect("F4_CLICKED", f4_pressed)  # ToolBar
    app.connect("F5_CLICKED", f5_pressed)  # ToolBar
    app.connect("F6_CLICKED", f6_pressed)  # ToolBar
    app.connect("F7_CLICKED", f7_pressed)  # ToolBar
    app.connect("F8_CLICKED", f8_pressed)  # ToolBar
    app.connect("F9_CLICKED", f9_pressed)  # ToolBar
    app.connect("F10_CLICKED", f10_pressed)  # ToolBar

    # app.connect('MOUSE_EVENT', on_click)  # Mouse Button
    app.connect("CURSES", handle_keys)  # Keyboard
    app.connect("SIGNALS", signal_event)  # Something it emit a signal
    app.connect("RESPONSE", response)  # Something it emit a signal

    app.connect("button-release-event", button_release_event)

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
