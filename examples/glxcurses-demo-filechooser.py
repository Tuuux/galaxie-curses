#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Curses Team, all rights reserved
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

import GLXCurses
import curses
import logging

# from GLXAudio.AudioSWHear import AudioSWHear


if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    video_file_extensions = (
        ".ts",
        ".mkv",
        ".3g2",
        ".3gp",
        ".asf",
        ".asx",
        ".avi",
        ".flv",
        ".m4v",
        ".mov",
        ".mp4",
        ".mpg",
        ".rm",
        ".srt",
        ".swf",
        ".vob",
        ".wmv",
    )

    # Create the main Application
    app = GLXCurses.Application()

    # Create a FileChooserMenu
    menu = GLXCurses.MenuBar()
    menu.info_label = "GLXCurses FileChooser Demo"

    label = GLXCurses.Label()
    label.__text = "Do you want to quit ?"
    label.xalign = 0.5
    label.yalign = 0.0

    # Create the main Window
    dialog = GLXCurses.Dialog()
    dialog.add(label)
    dialog.title = "Galaxie Dialog Demo"
    dialog.add_buttons("Yes", "1", "No", "2")
    dialog.set_default_response("1")

    # Create the FileChooser
    filechooser = GLXCurses.FileSelect()
    filechooser.set_decorated(True)

    filechooser_1 = GLXCurses.FileSelect()
    filechooser_1.set_decorated(True)

    hbox_main = GLXCurses.HBox()
    hbox_main.set_decorated(False)
    hbox_main.pack_start(filechooser)
    hbox_main.pack_start(filechooser_1)

    # Create a main Vertical Box
    # vbox_main = GLXCurses.VBox()
    # vbox_main.set_decorated(False)
    # vbox_main.pack_start(filechooser)

    # Create the main Window
    win_main = GLXCurses.Window()
    win_main.set_decorated(False)
    win_main.add(hbox_main)

    toolbar = GLXCurses.ToolBar()
    toolbar.labels = ["Help", "File Ext", "Decorated", "", "", "", "", "", "", "Quit"]

    # Create a Status Bar
    statusbar = GLXCurses.StatusBar()
    signal_context_id = statusbar.get_context_id("SIGNAL")
    curses_context_id = statusbar.get_context_id("CURSES")

    def handle_keys(self, event_signal, *event_args):
        statusbar.remove_all(curses_context_id)
        statusbar.push(curses_context_id, "HANDLE KEY: " + str(event_args[0]))

        if app.has_focus is None:

            if event_args[0] == curses.KEY_F1:
                pass
            if event_args[0] == curses.KEY_F2:
                if filechooser.get_app_file_extensions() is None:
                    filechooser.set_app_file_extensions(video_file_extensions)
                else:
                    filechooser.set_app_file_extensions(None)
            if event_args[0] == curses.KEY_F3:
                filechooser.set_decorated(not filechooser.get_decorated())
            if event_args[0] == curses.KEY_F4:
                pass
            if event_args[0] == curses.KEY_F5:
                pass
            if event_args[0] == curses.KEY_F6:
                pass
            if event_args[0] == curses.KEY_F7:
                pass
            if event_args[0] == curses.KEY_F8:
                pass
            if event_args[0] == curses.KEY_F9:
                pass
            if event_args[0] == curses.KEY_F10:
                # Everything have a end, the main loop too ...
                GLXCurses.mainloop.stop()

            # Keyboard temporary thing
            if event_args[0] == ord("q"):
                # Everything have a end, the main loop too ...
                GLXCurses.mainloop.stop()

    def on_click(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()

        if event_args["id"] == dialog.get_widget_for_response("1").id:
            dialog.close()
            GLXCurses.mainloop.stop()

        if event_args["id"] == dialog.get_widget_for_response("2").id:
            dialog.close()

    def f1_pressed(self, event_signal, event_args=None):
        pass

    def f2_pressed(self, event_signal, event_args=None):
        if filechooser.get_app_file_extensions() is None:
            filechooser.set_app_file_extensions(video_file_extensions)
        else:
            filechooser.set_app_file_extensions(None)

    def f3_pressed(self, event_signal, event_args=None):
        filechooser.set_decorated(not filechooser.get_decorated())

    def f4_pressed(self, event_signal, event_args=None):
        pass

    def f5_pressed(self, event_signal, event_args=None):
        pass

    def f6_pressed(self, event_signal, event_args=None):
        pass

    def f7_pressed(self, event_signal, event_args=None):
        pass

    def f8_pressed(self, event_signal, event_args=None):
        pass

    def f9_pressed(self, event_signal, event_args=None):
        pass

    def f10_pressed(self, event_signal, event_args=None):
        dialog.run()

    def signal_event(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()

        # Crash AUTO
        # statusbar.push(
        #    signal_context_id, "{0}: {1}".format(event_signal, event_args)
        # )

    # Add Everything inside the Application
    app.menubar = menu
    app.add_window(dialog)
    app.add_window(win_main)

    app.statusbar = statusbar
    app.toolbar = toolbar
    app.has_default = toolbar

    # Event's and Signals
    app.connect("F1_PRESSED", f1_pressed)  # ToolBar
    app.connect("F2_PRESSED", f2_pressed)  # ToolBar
    app.connect("F3_PRESSED", f3_pressed)  # ToolBar
    app.connect("F4_PRESSED", f4_pressed)  # ToolBar
    app.connect("F5_PRESSED", f5_pressed)  # ToolBar
    app.connect("F6_PRESSED", f6_pressed)  # ToolBar
    app.connect("F7_PRESSED", f7_pressed)  # ToolBar
    app.connect("F8_PRESSED", f8_pressed)  # ToolBar
    app.connect("F9_PRESSED", f9_pressed)  # ToolBar
    app.connect("F10_PRESSED", f10_pressed)  # ToolBar

    app.connect("F1_CLICKED", f1_pressed)  # ToolBar
    app.connect("F2_CLICKED", f2_pressed)  # ToolBar
    app.connect("F3_CLICKED", f3_pressed)  # ToolBar
    app.connect("F4_CLICKED", f4_pressed)  # ToolBar
    app.connect("F5_CLICKED", f5_pressed)  # ToolBar
    app.connect("F6_CLICKED", f6_pressed)  # ToolBar
    app.connect("F7_CLICKED", f7_pressed)  # ToolBar
    app.connect("F8_CLICKED", f8_pressed)  # ToolBar
    app.connect("F9_CLICKED", f9_pressed)  # ToolBar
    app.connect("F10_CLICKED", f10_pressed)  # ToolBar
    app.connect("BUTTON1_CLICKED", on_click)  # Mouse Button
    app.connect("BUTTON1_RELEASED", on_click)  # Mouse Button
    # app.connect('MOUSE_EVENT', on_click)  # Mouse Button
    app.connect("CURSES", handle_keys)  # Keyboard
    app.connect("SIGNALS", signal_event)  # Something it emit a signal

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
