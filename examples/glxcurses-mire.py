#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
# import GLXCurses
# from GLXCurses import GLXC
from PIL import Image, ImageDraw


def draw_colors_line(width=80, height=30, line=0, colors=None):
    if colors is None:
        colors = []
    xpos = 0
    ypos = height / 3 * line
    count = 1
    num_color = len(colors)
    for color in colors:
        if xpos == 0:
            x = width / num_color
        else:
            x = xpos
        if xpos == 0:
            y = height / 3
        else:
            y = ypos
        draw.rectangle(
            (xpos,
             ypos,
             x * count,
             height / 3),
            fill=color,
            outline=None
        )
        xpos += width / num_color
        count += 1


# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Curses Team, all rights reserved
if __name__ == '__main__':
    w = 640
    h = 480
    colors_dim = [
        (0, 0, 0),
        (128, 0, 0),
        (0, 128, 0),
        (170, 85, 0),
        (0, 0, 170),
        (170, 0, 170),
        (0, 170, 170),
        (170, 170, 170),
    ]
    colors_normal = [
        (85, 85, 85),
        (170, 0, 0),
        (0, 170, 0),
        (229, 229, 16),
        (0, 0, 170),
        (170, 0, 170),
        (0, 170, 170),
        (170, 170, 170),
    ]
    colors_bright = [
        (128, 85, 85),
        (255, 0, 0),
        (85, 255, 85),
        (255, 255, 85),
        (85, 85, 255),
        (255, 85, 255),
        (85, 255, 255),
        (255, 255, 255),
    ]

    num_lum = 3

    im = Image.new('RGB', (w, h), (0, 0, 0))
    draw = ImageDraw.Draw(im)
    draw_colors_line(
        width=w,
        height=h,
        line=0,
        colors=colors_dim
    )

    # draw_colors_line(
    #     width=w,
    #     height=h,
    #     line=1,
    #     colors=colors_normal
    # )

    draw_colors_line(
        width=w,
        height=h,
        line=3,
        colors= colors_bright
    )

    im.show()
