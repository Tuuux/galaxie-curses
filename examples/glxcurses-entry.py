#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Curses Team, all rights reserved
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

import GLXCurses
import curses
import logging

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    # Create the main Application
    app = GLXCurses.Application()

    # Creat a Status Bar
    toolbar = GLXCurses.ToolBar()
    toolbar.labels = [
        "Help",
        "Visibility",
        "Can Focus",
        "Prelight",
        "Selected",
        "Insensitive",
        "",
        "",
        "Menu",
        "Quit",
    ]

    # Create a Menu
    menu = GLXCurses.MenuBar()
    menu.info_label = "GLXCurses Entry Demo"

    entrybuffer_login = GLXCurses.EntryBuffer()
    entrybuffer_password = GLXCurses.EntryBuffer()

    # Create a Entry thing
    entry_login = GLXCurses.Entry()
    entry_login.set_buffer(entrybuffer_login)
    entry_login.set_text("Here you login")
    entry_login.set_completion(GLXCurses.EntryCompletion())
    entry_login.justify = "LEFT"
    entry_login.position_type = GLXCurses.GLXC.POS_BOTTOM

    entry_password = GLXCurses.Entry()
    entry_password.set_buffer(entrybuffer_password)
    entry_password.set_text("Here you password")
    entry_password.set_visibility(False)
    entry_password.justify = "LEFT"
    entry_password.position_type = GLXCurses.GLXC.POS_TOP

    label_login = GLXCurses.Label()
    label_login.set_markdown("**Username** :")
    label_login.set_single_line_mode(True)
    label_login.yalign = 1.0
    label_login.xalign = 1.0

    label_password = GLXCurses.Label()
    label_password.set_markdown("**Password** :")
    label_password.set_single_line_mode(True)
    label_password.yalign = 0.0
    label_password.xalign = 1.0

    # Create a new Horizontal Box container
    hbox_login = GLXCurses.HBox()
    hbox_login.spacing = 0
    hbox_login.pack_end(label_login, False)
    hbox_login.pack_end(entry_login, True)

    hbox_password = GLXCurses.HBox()
    hbox_password.spacing = 0
    hbox_password.pack_end(label_password, False)
    hbox_password.pack_end(entry_password, True)

    # Create a Horizontal Separator and a Label
    hline = GLXCurses.HSeparator()

    # Create a main Vertical Box
    vbox_main = GLXCurses.VBox()

    vbox_main.pack_end(hbox_login, True)
    vbox_main.pack_end(hbox_password, True)

    # Create the main Window
    win_main = GLXCurses.Window()
    win_main.decorated = True
    win_main.title = "Login"
    win_main.add(vbox_main)
    win_main.set_decorated(True)

    # Create a Status Bar
    statusbar = GLXCurses.StatusBar()
    signal_context_id = statusbar.get_context_id("SIGNAL")
    curses_context_id = statusbar.get_context_id("CURSES")


    def handle_keys(self, event_signal, *event_args):
        statusbar.remove_all(curses_context_id)
        statusbar.push(curses_context_id, "HANDLE KEY: " + str(event_args[0]))

        if app.has_focus is None:

            if event_args[0] == curses.KEY_UP:
                label_press_q.yalign -= 0.033

            if event_args[0] == curses.KEY_DOWN:
                label_press_q.yalign += 0.033

            if event_args[0] == curses.KEY_RIGHT:
                label_press_q.xalign += 0.033

            if event_args[0] == curses.KEY_LEFT:
                label_press_q.xalign -= 0.033

            # Keyboard temporary thing
            if event_args[0] == ord("q"):
                # Everything have a end, the main loop too ...
                GLXCurses.mainloop.stop()


    def on_click(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()
        if event_args["id"] == entry_login.id:
            statusbar.remove_all(signal_context_id)
            statusbar.push(signal_context_id, event_args["name"] + " " + event_signal)


    def f1_pressed(self, event_signal, event_args=None):
        pass


    def f2_pressed(self, event_signal, event_args=None):
        entry_password.set_visibility(not entry_password.visibility)


    def f3_pressed(self, event_signal, event_args=None):
        entry_password.can_focus = not entry_password.can_focus
        entry_password.set_editable(not entry_password.get_editable())


    def f4_pressed(self, event_signal, event_args=None):
        pass


    def f5_pressed(self, event_signal, event_args=None):

        if entry_password.get_selection_bounds():
            entry_password.select_region()
            entry_password._grab_focus()
        else:
            entry_password.select_region(
                start_pos=0, end_pos=len(entry_password.get_text())
            )
            entry_password._grab_focus()


    def f6_pressed(self, event_signal, event_args=None):
        pass


    def f7_pressed(self, event_signal, event_args=None):
        pass


    def f8_pressed(self, event_signal, event_args=None):
        pass


    def f9_pressed(self, event_signal, event_args=None):
        pass


    def f10_pressed(self, event_signal, event_args=None):
        GLXCurses.mainloop.stop()


    def signal_event(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()

        # Crash AUTO
        # statusbar.push(
        #    signal_context_id, "{0}: {1}".format(event_signal, event_args)
        # )


    # Add Everything inside the Application
    app.menubar = menu
    app.add_window(win_main)
    # app.remove_window(win_main)
    app.statusbar = statusbar
    app.toolbar = toolbar

    # Event's and Signals
    # Event's and Signals
    app.connect("F1_PRESSED", f1_pressed)  # ToolBar
    app.connect("F2_PRESSED", f2_pressed)  # ToolBar
    app.connect("F3_PRESSED", f3_pressed)  # ToolBar
    app.connect("F4_PRESSED", f4_pressed)  # ToolBar
    app.connect("F5_PRESSED", f5_pressed)  # ToolBar
    app.connect("F6_PRESSED", f6_pressed)  # ToolBar
    app.connect("F7_PRESSED", f7_pressed)  # ToolBar
    app.connect("F8_PRESSED", f8_pressed)  # ToolBar
    app.connect("F9_PRESSED", f9_pressed)  # ToolBar
    app.connect("F10_PRESSED", f10_pressed)  # ToolBar

    app.connect("F1_CLICKED", f1_pressed)  # ToolBar
    app.connect("F2_CLICKED", f2_pressed)  # ToolBar
    app.connect("F3_CLICKED", f3_pressed)  # ToolBar
    app.connect("F4_CLICKED", f4_pressed)  # ToolBar
    app.connect("F5_CLICKED", f5_pressed)  # ToolBar
    app.connect("F6_CLICKED", f6_pressed)  # ToolBar
    app.connect("F7_CLICKED", f7_pressed)  # ToolBar
    app.connect("F8_CLICKED", f8_pressed)  # ToolBar
    app.connect("F9_CLICKED", f9_pressed)  # ToolBar

    app.connect("BUTTON1_CLICKED", on_click)  # Mouse Button
    # app.connect('BUTTON1_RELEASED', on_click)  # Mouse Button
    # app.connect('MOUSE_EVENT', on_click)  # Mouse Button
    app.connect("CURSES", handle_keys)  # Keyboard
    app.connect("SIGNALS", signal_event)  # Something it emit a signal

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
