#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXCurses
import logging
import curses

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = "Tuux"

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    # Create the main Application
    app = GLXCurses.Application()

    # Create a Window
    win_main1 = GLXCurses.Window()
    win_main1.decorated = True
    win_main1.title = "Hello Container 1"
    win_main1.debug = False

    win_main2 = GLXCurses.Window()
    win_main2.decorated = True
    win_main2.title = "Hello Container 2"

    win_main3 = GLXCurses.Window()
    win_main3.decorated = True
    win_main3.title = "Hello Container 3"

    win_main4 = GLXCurses.Window()
    win_main4.decorated = True
    win_main4.title = "Hello Container 4"

    win_main5 = GLXCurses.Window()
    win_main5.decorated = True
    win_main5.title = "Hello Container 5"

    frame1 = GLXCurses.Frame()
    frame1.set_label("Galaxie")
    frame1.set_label_align(0.0, 0.0)
    # frame1.set_debug(True)

    filechooser1 = GLXCurses.FileSelect()
    filechooser1.set_decorated(True)
    filechooser2 = GLXCurses.FileSelect()
    filechooser2.set_decorated(True)
    filechooser3 = GLXCurses.FileSelect()
    filechooser3.set_decorated(True)

    hbox = GLXCurses.HBox()
    # hbox.set_debug(True)

    # hbox.pack_end(filechooser1)
    # hbox.pack_end(filechooser2)

    # win_main3.add(filechooser2)
    # win_main4.add(filechooser3)
    hbox.pack_end(win_main2, False)
    hbox.pack_end(win_main3, True)
    hbox.pack_end(frame1, False)
    hbox.pack_end(filechooser1, True)


    # label = GLXCurses.Label()
    # label.set_text('Bonjour')

    label_press_q = GLXCurses.Label()
    label_press_q.text = "Hello"
    # label_press_q.override_color('yellow')

    # frame1.add(filechooser1)
    # win_main3.add(frame1)
    # win_main2.add(win_main3)
    # win_main2.add(win_main3)
    win_main1.add(hbox)

    # hbox1 = GLXCurses.HBox()

    # frame1.add(hbox1)
    toolbar = GLXCurses.ToolBar()
    toolbar.labels = [
        "Help",
        "Cont 1 Box",
        "Cont 2 Box",
        "Cont 3 Box",
        "Cont 3 Box",
        "FileCh1 Box",
        "FileCh2 Box",
        "FileCh3 Box",
        "",
        "Quit",
    ]

    def f1_pressed(self, event_signal, event_args=None):
        pass

    def f2_pressed(self, event_signal, event_args=None):
        win_main1.decorated = not win_main1.decorated

    def f3_pressed(self, event_signal, event_args=None):
        win_main2.decorated = not win_main2.decorated

    def f4_pressed(self, event_signal, event_args=None):
        win_main3.decorated = not win_main3.decorated

    def f5_pressed(self, event_signal, event_args=None):

        frame1.set_decorated(not frame1.get_decorated())

    def f6_pressed(self, event_signal, event_args=None):
        filechooser1.set_decorated(not filechooser1.get_decorated())

    def f7_pressed(self, event_signal, event_args=None):
        filechooser2.set_decorated(not filechooser2.get_decorated())

    def f8_pressed(self, event_signal, event_args=None):
        filechooser3.set_decorated(not filechooser3.get_decorated())

    def f9_pressed(self, event_signal, event_args=None):
        pass

    def f10_pressed(self, event_signal, event_args=None):
        GLXCurses.mainloop.stop()

    def handle_keys(self, event_signal, *event_args):
        if event_args[0] == ord("q"):
            # Everything have a end, the main loop too ...
            GLXCurses.mainloop.stop()

    # Add Everything inside the Application
    app.add_window(win_main1)
    app.toolbar = toolbar
    app.has_default = toolbar

    # Signal
    # Event's and Signals
    app.connect("F1_PRESSED", f1_pressed)  # ToolBar
    app.connect("F2_PRESSED", f2_pressed)  # ToolBar
    app.connect("F3_PRESSED", f3_pressed)  # ToolBar
    app.connect("F4_PRESSED", f4_pressed)  # ToolBar
    app.connect("F5_PRESSED", f5_pressed)  # ToolBar
    app.connect("F6_PRESSED", f6_pressed)  # ToolBar
    app.connect("F7_PRESSED", f7_pressed)  # ToolBar
    app.connect("F8_PRESSED", f8_pressed)  # ToolBar
    app.connect("F9_PRESSED", f9_pressed)  # ToolBar
    app.connect("F10_PRESSED", f10_pressed)  # ToolBar

    app.connect("F1_CLICKED", f1_pressed)  # ToolBar
    app.connect("F2_CLICKED", f2_pressed)  # ToolBar
    app.connect("F3_CLICKED", f3_pressed)  # ToolBar
    app.connect("F4_CLICKED", f4_pressed)  # ToolBar
    app.connect("F5_CLICKED", f5_pressed)  # ToolBar
    app.connect("F6_CLICKED", f6_pressed)  # ToolBar
    app.connect("F7_CLICKED", f7_pressed)  # ToolBar
    app.connect("F8_CLICKED", f8_pressed)  # ToolBar
    app.connect("F9_CLICKED", f9_pressed)  # ToolBar
    app.connect("F10_CLICKED", f10_pressed)  # ToolBar

    app.connect("CURSES", handle_keys)

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
