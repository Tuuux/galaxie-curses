#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXCurses
import curses
import logging

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = "Tuux"

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    # Create the main Application
    app = GLXCurses.Application()

    # Create a Menu
    menu = GLXCurses.MenuBar()
    menu.info_label = "GLXCurses Adjustement Demo"

    # Create Buttons
    # value, lower, upper, step_increment, page_increment, page_size
    # value, lower, upper, step_increment, page_increment, page_size
    ButtonValueUp = GLXCurses.Button()
    ButtonValueUp.text = "value +"

    ButtonLowerUp = GLXCurses.Button()
    ButtonLowerUp.text = "lower +"

    ButtonUpperUp = GLXCurses.Button()
    ButtonUpperUp.text = "upper +"

    ButtonStepIncrementUp = GLXCurses.Button()
    ButtonStepIncrementUp.text = "step_increment +"

    ButtonPageIncrementUp = GLXCurses.Button()
    ButtonPageIncrementUp.text = "page_increment +"

    ButtonPageSizeUp = GLXCurses.Button()
    ButtonPageSizeUp.text = "page_size +"

    ######################
    ButtonValueDown = GLXCurses.Button()
    ButtonValueDown.text = "value -"

    ButtonLowerDown = GLXCurses.Button()
    ButtonLowerDown.text = "lower -"

    ButtonUpperDown = GLXCurses.Button()
    ButtonUpperDown.text = "upper -"

    ButtonStepIncrementDown = GLXCurses.Button()
    ButtonStepIncrementDown.text = "step_increment -"

    ButtonPageIncrementDown = GLXCurses.Button()
    ButtonPageIncrementDown.text = "page_increment -"

    ButtonPageSizeDown = GLXCurses.Button()
    ButtonPageSizeDown.text = "page_size -"

    # Create a new Horizontal Box contener
    hbox_line_1 = GLXCurses.HBox()
    hbox_line_1.spacing = 0

    hbox_line_1.pack_end(ButtonValueUp)
    hbox_line_1.pack_end(ButtonLowerUp)
    hbox_line_1.pack_end(ButtonUpperUp)
    hbox_line_1.pack_end(ButtonStepIncrementUp)
    hbox_line_1.pack_end(ButtonPageIncrementUp)
    hbox_line_1.pack_end(ButtonPageSizeUp)

    # Create a new Horizontal Box contener
    hbox_line_2 = GLXCurses.HBox()
    hbox_line_2.spacing = 0

    hbox_line_2.pack_end(ButtonValueDown)
    hbox_line_2.pack_end(ButtonLowerDown)
    hbox_line_2.pack_end(ButtonUpperDown)
    hbox_line_2.pack_end(ButtonStepIncrementDown)
    hbox_line_2.pack_end(ButtonPageIncrementDown)
    hbox_line_2.pack_end(ButtonPageSizeDown)

    # Create a Horizontal Separator and a Label
    hline = GLXCurses.HSeparator()

    label_press_q = GLXCurses.Label()
    label_press_q.text = 'Press "q" key to exit ...'
    label_press_q.set_justify("CENTER")
    label_press_q.xalign = 0.0
    label_press_q.yalign = 0.5
    label_press_q.override_color("yellow")

    label_adjustement = GLXCurses.Label()
    label_adjustement.set_justify("CENTER")
    label_adjustement.xalign = 0.0
    label_adjustement.yalign = 0.5
    label_adjustement.override_color("yellow")

    label_adjustement.text = "coucou"

    # Creat the adjustement
    adjustement = GLXCurses.Adjustment()

    def update_label():
        label_adjustement.text = ""
        "value: {0}, lower: {1}, upper: {2}, step_increment: {3}, "
        "page_increment: {4}, page_size: {5}, min_incr: {6}".format(
            adjustement.get_value(),
            adjustement.get_lower(),
            adjustement.get_upper(),
            adjustement.get_step_increment(),
            adjustement.get_page_increment(),
            adjustement.get_page_size(),
            adjustement.get_minimum_increment(),
        )


update_label()

# Create a main Vertical Box
vbox_main = GLXCurses.VBox()
vbox_main.pack_end(label_adjustement)
vbox_main.pack_end(hline)
vbox_main.pack_end(hbox_line_1)
vbox_main.pack_end(hbox_line_2)
vbox_main.pack_end(hline)
vbox_main.pack_end(label_press_q)

# Create the main Window
win_main = GLXCurses.Window()
win_main.add(vbox_main)
win_main.title = "GLXCurses Adjustement Demo"

# Create a Status Bar
statusbar = GLXCurses.StatusBar()
context_id = statusbar.get_context_id("example")


def handle_keys(self, event_signal, *event_args):
    logging.debug("HANDLE KEY: " + str(event_args[0]))

    if event_args[0] == curses.KEY_F5:
        app.has_focus = ButtonValueUp

    if event_args[0] == curses.KEY_F6:
        ButtonValueUp.sensitive = not ButtonValueUp.sensitive

    if event_args[0] == curses.KEY_UP:
        label_press_q.yalign -= 0.033

    if event_args[0] == curses.KEY_DOWN:
        label_press_q.yalign += 0.033

    if event_args[0] == curses.KEY_RIGHT:
        label_press_q.xalign += 0.033

    if event_args[0] == curses.KEY_LEFT:
        label_press_q.xalign -= 0.033

    # Keyboard temporary thing
    if event_args[0] == ord("q"):
        # Everything have a end, the main loop too ...
        GLXCurses.mainloop.stop()


def on_click(self, event_signal, event_args=None):
    if event_args is None:
        event_args = dict()
    statusbar.push(context_id, "")
    # Value
    if event_args["id"] == ButtonValueUp.id:
        adjustement.set_value(
            adjustement.get_value() + adjustement.get_step_increment()
        )
        statusbar.push(context_id, "Value is set to " + str(adjustement.get_value()))
        update_label()
    if event_args["id"] == ButtonValueDown.id:
        adjustement.set_value(
            adjustement.get_value() - adjustement.get_step_increment()
        )
        statusbar.push(context_id, "Value is set to " + str(adjustement.get_value()))
        update_label()
    # Lower
    if event_args["id"] == ButtonLowerUp.id:
        adjustement.set_lower(
            adjustement.get_lower() + adjustement.get_step_increment()
        )
        statusbar.push(context_id, "Lower is set to " + str(adjustement.get_lower()))
        update_label()
    if event_args["id"] == ButtonLowerDown.id:
        adjustement.set_lower(
            adjustement.get_lower() - adjustement.get_step_increment()
        )
        statusbar.push(context_id, "Lower is set to " + str(adjustement.get_lower()))
        update_label()
    # Upper
    if event_args["id"] == ButtonUpperUp.id:
        adjustement.set_upper(
            adjustement.get_upper() + adjustement.get_step_increment()
        )
        statusbar.push(context_id, "Upper is set to " + str(adjustement.get_upper()))
        update_label()
    if event_args["id"] == ButtonUpperDown.id:
        adjustement.set_upper(
            adjustement.get_upper() - adjustement.get_step_increment()
        )
        statusbar.push(context_id, "Upper is set to " + str(adjustement.get_upper()))
        update_label()
    # Step Increment
    if event_args["id"] == ButtonStepIncrementUp.id:
        adjustement.set_step_increment(adjustement.get_step_increment() + 0.1)
        statusbar.push(
            context_id,
            "Step Increment is set to " + str(adjustement.get_step_increment()),
        )
        update_label()
    if event_args["id"] == ButtonStepIncrementDown.id:
        adjustement.set_step_increment(adjustement.get_step_increment() - 0.1)
        statusbar.push(
            context_id,
            "Step Increment is set to " + str(adjustement.get_step_increment()),
        )
        update_label()
    # Page Increment
    if event_args["id"] == ButtonPageIncrementUp.id:
        adjustement.set_page_increment(adjustement.get_page_increment() + 0.1)
        statusbar.push(
            context_id,
            "Page Increment is set to " + str(adjustement.get_page_increment()),
        )
        update_label()
    if event_args["id"] == ButtonPageIncrementDown.id:
        adjustement.set_page_increment(adjustement.get_page_increment() - 0.1)
        statusbar.push(
            context_id,
            "Page Increment is set to " + str(adjustement.get_page_increment()),
        )
        update_label()

    # Page Size
    if event_args["id"] == ButtonPageSizeUp.id:
        adjustement.set_page_size(
            adjustement.get_page_size() + adjustement.get_page_increment()
        )
        statusbar.push(
            context_id, "Page Size is set to " + str(adjustement.get_page_size())
        )
        update_label()
    if event_args["id"] == ButtonPageSizeDown.id:
        adjustement.set_page_size(
            adjustement.get_page_size() - adjustement.get_page_increment()
        )
        statusbar.push(
            context_id, "Page Size is set to " + str(adjustement.get_page_size())
        )
        update_label()


def on_double_click(event_signal, event_args=None):
    if event_args is None:
        event_args = dict()
    on_click(event_signal, event_args)
    on_click(event_signal, event_args)


# Add Everything inside the Application
app.menubar = menu
app.add_window(win_main)
app.statusbar = statusbar
# Signal
app.connect("BUTTON1_CLICKED", on_click)
app.connect("BUTTON1_RELEASED", on_click)
app.connect("BUTTON1_DOUBLE_CLICKED", on_click)
app.connect("BUTTON1_TRIPLE_CLICKED", on_click)
app.connect("CURSES", handle_keys)

# Main loop
GLXCurses.mainloop.start()

# THE END
sys.exit(0)
