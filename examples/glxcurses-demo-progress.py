#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXCurses
import curses
import logging

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = "Tuux"

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    # Create the main Application
    app = GLXCurses.Application()

    menu = GLXCurses.MenuBar()
    menu.info_label = "GLXCurses ProgressBar Demo"

    # GREEN
    progress_bar_1 = GLXCurses.ProgressBar()
    progress_bar_1.value = 25
    progress_bar_1.foreground_color_normal = (0, 255, 0)


    # YELLOW
    progress_bar_2 = GLXCurses.ProgressBar()
    progress_bar_2.value = 50
    progress_bar_2.foreground_color_normal = (255, 255, 0)

    # RED
    progress_bar_3 = GLXCurses.ProgressBar()
    progress_bar_3.value = 75
    progress_bar_3.foreground_color_normal = (255, 0, 0)


    # DEFAULT (WHITE)
    progress_bar_4 = GLXCurses.ProgressBar()
    progress_bar_4.value = 100


    # Create a main Vertical Box
    vbox_main = GLXCurses.VBox()
    vbox_main.pack_end(progress_bar_1)
    vbox_main.pack_end(progress_bar_2)
    vbox_main.pack_end(progress_bar_3)
    vbox_main.pack_end(progress_bar_4)


    # Create the main Window
    win_main = GLXCurses.Window()
    # win_main.title = "GLXCurses ProgressBar Demo"
    win_main.add(vbox_main)

    # Creat a Status Bar
    toolbar = GLXCurses.ToolBar()
    toolbar.labels = ["Green +", "Green -", "Yellow +", "Yellow -", "Red +", "Red -", "White +", "White -", "", "Quit"]

    def handle_keys(self, event_signal, *event_args):
        logging.debug("HANDLE KEY: " + str(event_args[0]))

        # Keyboard temporary thing
        if event_args[0] == ord("q"):
            # Everything have a end, the main loop too ...
            GLXCurses.mainloop.stop()

    def f1_pressed(self, event_signal, event_args=None):
        progress_bar_1.value += 1

    def f2_pressed(self, event_signal, event_args=None):
        progress_bar_1.value -= 1

    def f3_pressed(self, event_signal, event_args=None):
        progress_bar_2.value += 1

    def f4_pressed(self, event_signal, event_args=None):
        progress_bar_2.value -= 1

    def f5_pressed(self, event_signal, event_args=None):
        progress_bar_3.value += 1

    def f6_pressed(self, event_signal, event_args=None):
        progress_bar_3.value -= 1

    def f7_pressed(self, event_signal, event_args=None):
        progress_bar_4.value += 1

    def f8_pressed(self, event_signal, event_args=None):
        progress_bar_4.value -= 1

    def f10_pressed(self, event_signal, event_args=None):
        # Everything have a end, the main loop too ...
        GLXCurses.mainloop.stop()

    # Add Everything inside the Application
    app.menubar = menu
    app.add_window(win_main)
    app.toolbar = toolbar

    app.has_default = toolbar
    app.has_focus = toolbar
    app.has_prelight = toolbar
    # Signal
    app.connect("CURSES", handle_keys)

    app.connect("F1_CLICKED", f1_pressed)  # ToolBar
    app.connect("F2_CLICKED", f2_pressed)  # ToolBar
    app.connect("F3_CLICKED", f3_pressed)  # ToolBar
    app.connect("F4_CLICKED", f4_pressed)  # ToolBar
    app.connect("F5_CLICKED", f5_pressed)  # ToolBar
    app.connect("F6_CLICKED", f6_pressed)  # ToolBar
    app.connect("F7_CLICKED", f7_pressed)  # ToolBar
    app.connect("F8_CLICKED", f8_pressed)  # ToolBar
    app.connect("F10_CLICKED", f10_pressed)  # ToolBar

    app.connect("F1_PRESSED", f1_pressed)  # ToolBar
    app.connect("F2_PRESSED", f2_pressed)  # ToolBar
    app.connect("F3_PRESSED", f3_pressed)  # ToolBar
    app.connect("F4_PRESSED", f4_pressed)  # ToolBar
    app.connect("F5_PRESSED", f5_pressed)  # ToolBar
    app.connect("F6_PRESSED", f6_pressed)  # ToolBar
    app.connect("F7_PRESSED", f7_pressed)  # ToolBar
    app.connect("F8_PRESSED", f8_pressed)  # ToolBar
    app.connect("F10_PRESSED", f10_pressed)  # ToolBar

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
