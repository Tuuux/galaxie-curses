#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXCurses
import logging

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = "Tuux"

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    # Create the main Application
    app = GLXCurses.Application()

    # Create a main Vertical Box
    vbox_main = GLXCurses.VBox()

    # Create the main Window
    win_main = GLXCurses.Window()
    win_main.add(vbox_main)

    menuitem = GLXCurses.MenuItem()
    menuitem.text = "_Bonjour that is my super menu item title"
    menuitem.text_short_cut = "Ctrl + a"
    menuitem.spacing = 1

    menuitem1 = GLXCurses.MenuItem()
    menuitem1.text = "_Hello.42 comment va tu "
    menuitem1.text_short_cut = "Ctrl + a"

    menuitem2 = GLXCurses.MenuItem()
    menuitem2.text = "H_ello.42 comment va tu "
    menuitem2.text_short_cut = "Ctrl + b"

    menuitem3 = GLXCurses.MenuItem()
    menuitem3.text = "He_llo.42 comment va tu "
    menuitem3.text_short_cut = "Ctrl + c"

    menuitem4 = GLXCurses.MenuItem()
    menuitem4.text = "Hel_lo.42 comment va tu "
    menuitem4.text_short_cut = "Ctrl + d"

    menuitem5 = GLXCurses.MenuItem()
    menuitem5.text = "Hell_o.42 comment va tu "
    menuitem5.text_short_cut = "Ctrl + e"

    menuitem6 = GLXCurses.MenuItem()
    menuitem6.text = "Hello_.42 comment va tu "
    menuitem6.text_short_cut = "Ctrl + f"

    menuitem7 = GLXCurses.MenuItem()
    menuitem7.text = "Hello._42 comment va tu "
    menuitem7.text_short_cut = "Ctrl + i"

    menuitem8 = GLXCurses.MenuItem()
    menuitem8.text = "Hello.4_2 comment va tu "
    menuitem8.text_short_cut = "Ctrl + j"

    menuitem9 = GLXCurses.MenuItem()
    menuitem9.text = "Hello.42 comment va tu "
    menuitem9.text_short_cut = "Ctrl + h"

    menu = GLXCurses.Menu()
    menu.decorated = True
    menu.pack_end(menuitem1)
    menu.pack_end(menuitem2)
    menu.pack_end(menuitem3)
    menu.pack_end(menuitem4)
    menu.pack_end(menuitem5)
    menu.pack_end(menuitem6)
    menu.pack_end(menuitem7)
    menu.pack_end(menuitem8)
    menu.x_offset = 20
    # menu.x = 5
    # menu.y = 5

    win_main.add(menu)

    def handle_keys(self, event_signal, *event_args):
        logging.debug("HANDLE KEY: " + str(event_args[0]))

        # Keyboard temporary thing
        if event_args[0] == ord("q"):
            # Everything have a end, the main loop too ...
            GLXCurses.mainloop.stop()

    # Add Everything inside the Application
    app.add_window(win_main)

    # Signal

    app.connect("CURSES", handle_keys)

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
