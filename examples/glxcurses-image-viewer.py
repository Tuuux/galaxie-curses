#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXCurses
import curses
import logging
from GLXCurses import GLXC

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = "Tuux"

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    # Create the main Application
    app = GLXCurses.Application()

    label = GLXCurses.Label().new("Do you want to quit ?")
    label.xalign = 0.5
    label.yalign = 0.5

    # Create the main Window
    dialog = GLXCurses.Dialog()
    dialog.add(label)
    dialog.title = "Galaxie Image Viewer"
    dialog.add_buttons("Yes", "1", "No", "2")
    dialog.set_default_response("1")

    image = GLXCurses.Image()
    image.path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "picture_01.png")
    image.load_image()
    image.to_data()

    about_label = GLXCurses.Label().new("GLXCurses is a Text Based Interface (TUI) Tool Kit (TK)")
    about_label.set_single_line_mode(False)
    about_label.yalign = 0.0

    about_dialog = GLXCurses.Dialog()
    about_dialog.add(about_label)
    about_dialog.title = "About Galaxie Image Viewer"
    about_dialog.add_buttons("Ok", "3")
    about_dialog.set_default_response("3")

    # dialog.get_widget_for_response(1)._set_state_prelight(True)

    toolbar = GLXCurses.ToolBar()
    toolbar.labels = ["Help",
                      "Width +",
                      "Width -",
                      "Height +",
                      "Height -",
                      "picture_01",
                      "picture_02",
                      "picture_03",
                      "picture_04",
                      "Quit"]

    window = GLXCurses.Window()
    window.add(image)

    def on_click(self, event_signal, event_args=None):
        if event_args["id"] == dialog.get_widget_for_response("1").id:
            dialog.close()
            GLXCurses.mainloop.stop()

        if event_args["id"] == dialog.get_widget_for_response("2").id:
            dialog.close()

        if event_args["id"] == about_dialog.get_widget_for_response("3").id:
            about_dialog.close()

    def handle_keys(self, event_signal, *event_args):
        logging.debug("HANDLE KEY: " + str(event_args[0]))

        # Keyboard temporary thing
        if event_args[0] == ord("q"):
            # Everything have a end, the main loop too ...
            GLXCurses.mainloop.stop()

        if event_args[0] == ord("c"):
            # Everything have a end, the main loop too ...
            dialog.close()

        if event_args[0] == ord("r"):
            # Everything have a end, the main loop too ...
            dialog.run()

    def signal_event(self, event_signal, event_args=None):
        logging.debug(str(event_args))
        if event_args[0] == 1:
            dialog.close()

        if event_args[0] == 3:
            about_dialog.close()
            app.active_window_id = window.id

        # Crash AUTO
        # statusbar.push(
        #    signal_context_id, "{0}: {1}".format(event_signal, event_args)
        # )

    def f1_pressed(self, event_signal, event_args=None):
        about_dialog.run()

    def f2_pressed(self, event_signal, event_args=None):
        image.width_zoom += 0.1
        image.to_data()

    def f3_pressed(self, event_signal, event_args=None):
        image.width_zoom -= 0.1
        image.to_data()

    def f4_pressed(self, event_signal, event_args=None):
        image.height_zoom -= 0.1
        image.to_data()

    def f5_pressed(self, event_signal, event_args=None):
        image.height_zoom += 0.1
        image.to_data()

    def f6_pressed(self, event_signal, event_args=None):
        image.path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "picture_02.png")
        image.load_image()
        image.to_data()

    def f7_pressed(self, event_signal, event_args=None):
        image.path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "picture_03.png")
        image.load_image()
        image.to_data()

    def f8_pressed(self, event_signal, event_args=None):
        image.path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "picture_04.png")
        image.load_image()
        image.to_data()

    def f9_pressed(self, event_signal, event_args=None):
        image.path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "officiallogo-nd-50.jpg")
        image.load_image()
        image.to_data()

    def f10_pressed(self, event_signal, event_args=None):
        dialog.run()

    # Add Everything inside the Application

    app.add_window(dialog)
    app.add_window(about_dialog)
    app.toolbar = toolbar
    app.add_window(window)
    app.has_default = toolbar

    # Signal
    # Event's and Signals
    app.connect("F1_PRESSED", f1_pressed)  # ToolBar
    app.connect("F2_PRESSED", f2_pressed)  # ToolBar
    app.connect("F3_PRESSED", f3_pressed)  # ToolBar
    app.connect("F4_PRESSED", f4_pressed)  # ToolBar
    app.connect("F5_PRESSED", f5_pressed)  # ToolBar
    app.connect("F6_PRESSED", f6_pressed)  # ToolBar
    app.connect("F7_PRESSED", f7_pressed)  # ToolBar
    app.connect("F8_PRESSED", f8_pressed)  # ToolBar
    app.connect("F9_PRESSED", f9_pressed)  # ToolBar
    app.connect("F10_PRESSED", f10_pressed)  # ToolBar

    app.connect("F1_CLICKED", f1_pressed)  # ToolBar
    app.connect("F2_CLICKED", f2_pressed)  # ToolBar
    app.connect("F3_CLICKED", f3_pressed)  # ToolBar
    app.connect("F4_CLICKED", f4_pressed)  # ToolBar
    app.connect("F5_CLICKED", f5_pressed)  # ToolBar
    app.connect("F6_CLICKED", f6_pressed)  # ToolBar
    app.connect("F7_CLICKED", f7_pressed)  # ToolBar
    app.connect("F8_CLICKED", f8_pressed)  # ToolBar
    app.connect("F9_CLICKED", f9_pressed)  # ToolBar
    app.connect("F10_CLICKED", f10_pressed)  # ToolBar

    app.connect("CURSES", handle_keys)
    app.connect("BUTTON1_CLICKED", on_click)  # Mouse Button
    app.connect("BUTTON1_RELEASED", on_click)
    app.connect("SIGNALS", signal_event)

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
