#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXCurses

if __name__ == '__main__':

    # Create the main Application
    app = GLXCurses.Application()

    # Create a Label
    label = GLXCurses.Label()
    label.set_markdown("_Hello_ **World** !!!")
    label.set_single_line_mode(True)

    # Create the main Window and add our Label inside
    win = GLXCurses.Window()
    win.add(label)


    def handle_keys(self, event_signal, *event_args):
        if event_args[0] == ord('q'):
            # Everything have a end, the main loop too ...
            # Control + C work automatically
            GLXCurses.mainloop.stop()


    # Add Everything inside the Application
    app.add_window(win)

    # Connect the application to Curses event
    app.connect('CURSES', handle_keys)

    # Main loop
    GLXCurses.mainloop.start()
