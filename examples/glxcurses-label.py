#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXCurses

import curses
import logging

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = "Tuux"

if __name__ == "__main__":
    logging.basicConfig(
        filename="/tmp/galaxie-curses.log",
        level=logging.DEBUG,
        format="%(asctime)s, %(levelname)s, %(message)s",
    )
    logging.info("Started glxcurses-demo")

    # Create the main Application
    app = GLXCurses.Application()

    # Create a Menu
    menu = GLXCurses.MenuBar()
    menu.info_label = "GLXCurses Label Demo"

    label_press_q = GLXCurses.Label()
    # label_press_q.label = 'Press "q" key to exit ... What about you arrows\'s key\'s'
    label_press_q.set_markdown(
        "**Press** \"q\" **k**e**y** to **e**x**i**t ... ++***What***++ ++about++ you *arrows's* key's"
    )
    label_press_q.set_single_line_mode(True)
    label_press_q.set_justify("RIGHT")
    label_press_q.yalign = 0.5
    # label_press_q.xalign = 0.5

    label_press_q.foreground_color_normal = (200, 200, 0)

    # Create a main Vertical Box
    vbox_main = GLXCurses.VBox()
    vbox_main.pack_end(label_press_q)

    # Create the main Window
    win_main = GLXCurses.Window()
    win_main.add(label_press_q)

    # Create a Status Bar
    statusbar = GLXCurses.StatusBar()
    context_id = statusbar.get_context_id("example")
    signal_context_id = statusbar.get_context_id("SIGNAL")
    button1_context_id = statusbar.get_context_id("BUTTON1")
    arrow_pressed_context_id = statusbar.get_context_id("ARROW_PRESSED")


    def handle_keys(self, event_signal, *event_args):
        statusbar.remove_all(arrow_pressed_context_id)
        if type(event_args[0]) == int and event_args[0] <= 255:
            text = "HANDLE KEY: {0} Char: {1}".format(event_args[0], chr(event_args[0]))
        else:
            text = "HANDLE KEY: {0}".format(event_args[0])
        statusbar.push(arrow_pressed_context_id, text)

        if event_args[0] == curses.KEY_UP:
            if label_press_q.yalign - 0.033 >= 0.0:
                label_press_q.yalign -= 0.033

        if event_args[0] == curses.KEY_DOWN:
            if label_press_q.yalign + 0.033 <= 1.0:
                label_press_q.yalign += 0.033

        if event_args[0] == curses.KEY_RIGHT:
            if label_press_q.xalign + 0.033 <= 1.0:
                label_press_q.xalign += 0.033
            elif label_press_q.xalign + 0.033 > 1.0:
                label_press_q.xalign = 1.0

        if event_args[0] == curses.KEY_LEFT:
            if label_press_q.xalign - 0.033 >= 0.0:
                label_press_q.xalign -= 0.033
            elif label_press_q.xalign - 0.033 < 0.0:
                label_press_q.xalign = 0.0

        # Keyboard temporary thing
        if event_args[0] == ord("q") or event_args[0] == "q":
            # Everything have a end, the main loop too ...
            GLXCurses.mainloop.stop()


    def on_click(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()


    def signal_event(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()

        # Crash AUTO
        # statusbar.push(
        #    signal_context_id, "{0}: {1}".format(event_signal, event_args)
        # )


    # Add Everything inside the Application
    app.menubar = menu
    app.add_window(win_main)
    # app.remove_window(win_main)
    app.statusbar = statusbar

    # Event's and Signals
    app.connect("BUTTON1_CLICKED", on_click)  # Mouse Button
    app.connect("BUTTON1_RELEASED", on_click)  # Mouse Button
    app.connect("CURSES", handle_keys)  # Keyboard
    app.connect("SIGNALS", signal_event)  # Something it emit a signal

    # Main loop
    GLXCurses.mainloop.start()

    # THE END
    sys.exit(0)
