==============
GALAXIE CURSES
==============
CHANGELOGS
----------
**0.3.6a1**
  * Permit ProgressBar to be draw
  * Migrate to setuptools setup.cfg file
  * Update Makefile
  * Migrate to direnv
  * Migrate CI/CD to TeamCity
  * Migrate to coverage
  * Update readthedocs settings

**0.3.5**
  * HBox VBox split area method update
  * Menu support no color mode
  * Fixe many thing on Entry() widget
  * Container's packend packstart add method update for permit expand property
  * Menu is functional
  * FileChooser Refactoring
  * FileChooser have color extension
  * FileChooser better link detection
  * FileChooser os.scandir() migration
  * Menu routing trouble fixe
  * Add Menu HSeparator capability
  * Fixe #4 with a more simple signal routing, now only Dialog install block signal dispatch
  * Fixe #8 by introduce usage of Window().type_hint property, actually only Dialog() use it
  * Menu refactoring
  * ``glxc-mc`` add new menu

**0.3.4**
  * Use external Galaxie EveLoop as Mainloop
  * Use external Galaxie EveLoop as EventBus
  * Add Image support
  * Best colors management
  * Menu (Bug with Mouse and Signals routing)
  * Integrate version 0.1.3 of glxeveloop

Know bug:
  * **Menu** Routing (The bus migration have let the event routing in wrong state)
  * **Entry** , the migration about handler signal is not finish

**0.3.3**
  * Fix issue #3 about examples files crash
  * Remove import.py for limit confusion
  * Remove vumeter example (will come back soon)
  * Add beta MenuBar Menu MenuItem system

**0.3.3rc1** - Mar 10 2020
  * MenuItem have background colorization

**0.3.2** - Mar 08 2020
  * Remove numpy dependency
  * Remove PyYAML dependency
  * When piperclip is not present the ClipBoard class use internal json format
  * Application is a true Single Point of Thruth then have a Classe for that
  * Introduction of Menu (Not functional yet)

**0.3.1** - Mar 03 2020
  * Fixe Dialog Overlap
  * Cleanup

**0.3** - Mar 02 2020
  * Smooth/Speed Resize
  * Mouse Wheel
  * Python3 port
  * Application refactoring
  * Introduse Area Class
  * Add Screen Class
  * Add FileChooser Class
  * Add Dialog Class
  * Add Entry Class
  * More UnitTest

**0.3rc1** - Mar 02 2020
  * Preparation for the 0.3

**release-0.2.0-soleil** - Jul 01 2018
  * First tag
  * First test pypi publication
  * First pypi publication
  * Who know ...