.PHONY: help
help:
	@echo  'make clean      - Remove every created directory and restart from scratch'
	@echo  'make docs       - Install Sphinx and requirements, then build documentations'
	@echo  'make prepare    - Easy way for make everything'
	@echo  'make tests      - Easy way for make tests over a CI'
	@echo  ''
	@echo  'sudo make install-python   - Easy way to install python3 pip and venv'

.PHONY: install-python
install-python:
	@ echo ""
	@ echo "**************************** INSTALL PYTHON PACKAGES ***************************"
	@ apt-get update && apt install -y python3 python3-pip python3-venv xclip

.PHONY: header
header:
	@echo "**************************** GALAXIE CURSES MAKEFILE ***************************"
	@echo "HOSTNAME	`uname -n`"
	@echo "KERNEL RELEASE `uname -r`"
	@echo "KERNEL VERSION `uname -v`"
	@echo "PROCESSOR	`uname -m`"
	@echo "********************************************************************************"

.PHONY: prepare
prepare: header
	@pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  OK  ] PIP3" || \
	echo "[FAILED] PIP3"

	@pip3 install -U --no-cache-dir --quiet -r requirements.txt &&\
	echo "[  OK  ] REQUIREMENTS" || \
	echo "[FAILED] REQUIREMENTS"

	@pip3 install -U -e . --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL GALAXIE-CURSES AS DEV MODE" || \
	echo "[FAILED] INSTALL GALAXIE-CURSES AS DEV MODE"

.PHONY: docs
docs:
	@ echo ""
	@ echo "***************************** BUILD DOCUMENTATIONS *****************************"
	@pip3 install -U --no-cache-dir --quiet -r docs/requirements.txt &&\
	echo "[  OK  ] DOCS REQUIREMENTS" || \
	echo "[FAILED] DOCS REQUIREMENTS"
	@cd $(PWD)/docs && make html

.PHONY: tests
tests:
	@ echo ""
	@ echo "********************************** START TESTS *********************************"
	@ coverage run -m unittest tests/test_*.py tests/libs/test_*.py
	@ coverage report -m

.PHONY: clean
clean: header
	@ 	echo ""
	@ 	echo "*********************************** CLEAN UP ***********************************"
	rm -rf ./venv
	rm -rf ./.direnv
	rm -rf ~/.cache/pip
	rm -rf ./.eggs
	rm -rf ./*.egg-info
	rm -rf .coverage
